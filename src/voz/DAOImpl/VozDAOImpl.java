package voz.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import voz.DAO.VozDAO;
import voz.model.Karta;
import voz.model.Voz;

public class VozDAOImpl implements VozDAO {

	private Connection conn;
	
	public VozDAOImpl(Connection conn) {
		this.conn = conn;
	}
	
	@Override
	public Voz get(String brojVoza) throws Exception{
		Voz voz = null;
		
		String sql = "SELECT v.id AS VozId, v.broj, v.naziv, v.datumIVremePolaska, v.cenaKarte, v.brojMesta,\r\n"
				+ "k.id AS KartaId, k.datumIVremeProdaje, k.kupac, k.razred FROM voz v\r\n"
				+ "LEFT JOIN karte k ON v.id = k.vozId WHERE v.broj = ?;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, brojVoza);
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long vId = rset.getLong(++kolona);
					String vBroj = rset.getString(++kolona);
					String vNaziv = rset.getString(++kolona);
					LocalDateTime vDatumIVremePolaska = rset.getTimestamp(++kolona).toLocalDateTime();
					double vCenaKarte = rset.getDouble(++kolona);
					int vBrojMesta = rset.getInt(++kolona);
					
					if (voz == null) {
						voz = new Voz(vId, vBroj, vNaziv, vDatumIVremePolaska, vCenaKarte, vBrojMesta);
					}
					
					long kId = rset.getLong(++kolona);
					if (kId != 0) {
						LocalDateTime kDatumIVremeProdaje = rset.getTimestamp(++kolona).toLocalDateTime();
						String kKupac = rset.getString(++kolona);
						int kRazred = rset.getInt(++kolona);

						Karta karta = new Karta(kId, voz, kDatumIVremeProdaje, kKupac, kRazred);
						voz.addKarta(karta);
					}
				}
			}
		}
		return voz;
	}

	@Override
	public Collection<Voz> getAll() throws Exception{
		Map<Long, Voz> vozMap = new LinkedHashMap<>();
		
		String sql = 
				"SELECT v.id AS VozId, v.broj, v.naziv, v.datumIVremePolaska, v.cenaKarte, v.brojMesta,\r\n"
				+ "k.id AS KartaId, k.datumIVremeProdaje, k.kupac, k.razred FROM voz v\r\n"
				+ "LEFT JOIN karte k ON v.id = k.vozId;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long vId = rset.getLong(++kolona);
					String vBroj = rset.getString(++kolona);
					String vNaziv = rset.getString(++kolona);
					LocalDateTime vDatumIVremePolaska = rset.getTimestamp(++kolona).toLocalDateTime();
					double vCenaKarte = rset.getDouble(++kolona);
					int vBrojMesta = rset.getInt(++kolona);
					
					Voz voz = vozMap.get(vId);
					if (voz == null) {
						voz = new Voz(vId, vBroj, vNaziv, vDatumIVremePolaska, vCenaKarte, vBrojMesta);
						vozMap.put(voz.getId(), voz);
					}
					
					long kId = rset.getLong(++kolona);
					if (kId != 0) {
						LocalDateTime kDatumIVremeProdaje = rset.getTimestamp(++kolona).toLocalDateTime();
						String kKupac = rset.getString(++kolona);
						int kRazred = rset.getInt(++kolona);

						Karta karta = new Karta(kId, voz, kDatumIVremeProdaje, kKupac, kRazred);
						voz.addKarta(karta);
					}
				}
			}
		}
		return vozMap.values();
	}
}
