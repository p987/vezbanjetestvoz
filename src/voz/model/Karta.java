package voz.model;

import java.time.LocalDateTime;
import java.util.Objects;

import voz.util.Konzola;


public class Karta {

	private final long id;
	private Voz voz;
	private LocalDateTime datumIVremeProdaje;
	private String kupac;
	private int razred;
	
	
	
	public Karta(long id, Voz voz, LocalDateTime datumIVremeProdaje, String kupac, int razred) {
		this.id = id;
		this.voz = voz;
		this.datumIVremeProdaje = datumIVremeProdaje;
		this.kupac = kupac;
		this.razred = razred;
	}


	@Override
	public String toString() {
		return "Karta [id=" + id + ", voz=" + voz + ", datumIVremeProdaje=" + Konzola.formatiraj(datumIVremeProdaje) + ", kupac=" + kupac
				+ ", razred=" + razred + "]";
	}


	public Voz getVoz() {
		return voz;
	}


	public void setVoz(Voz voz) {
		this.voz = voz;
	}


	public LocalDateTime getDatumIVremeProdaje() {
		return datumIVremeProdaje;
	}


	public void setDatumIVremeProdaje(LocalDateTime datumIVremeProdaje) {
		this.datumIVremeProdaje = datumIVremeProdaje;
	}


	public String getKupac() {
		return kupac;
	}


	public void setKupac(String kupac) {
		this.kupac = kupac;
	}


	public int getRazred() {
		return razred;
	}


	public void setRazred(int razred) {
		this.razred = razred;
	}


	public long getId() {
		return id;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Karta other = (Karta) obj;
		return id == other.id;
	}

}
