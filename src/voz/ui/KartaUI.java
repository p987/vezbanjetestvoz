package voz.ui;

import java.time.LocalDateTime;

import voz.DAO.KartaDAO;
import voz.model.Karta;
import voz.model.Voz;
import voz.util.Konzola;

public class KartaUI {

	private static KartaDAO kartaDAO;
	
	public static void setKartaDAO(KartaDAO kartaDAO) {
		KartaUI.kartaDAO = kartaDAO;
	}

	public static void dodavanje() {
		try {
			Voz voz = VozUI.pronalazenje();
			
			if (voz == null) {
				return;
			}
			if ((voz.getBrojMesta()-voz.getKartaSet().size()) <= 0) { // pr. mesta(5) - size(5) <= 0
				System.out.println("Nema vise slobodnih mesta!");
				return;
			}
			if ((voz.getDatumIVremePolaska().isBefore(LocalDateTime.now()))) {
				System.out.println("Voz je vec posao datuma: " + Konzola.formatiraj(voz.getDatumIVremePolaska()));
				return;
			}
			int razred = Konzola.ocitajInt("Unesite razred(1 ili 2) voza: ");
			if ((razred != 1) && (razred != 2)) {
				System.out.println("Voz moze da bude 1 ili 2 razred");
				return;
			}
			String kupac = Konzola.ocitajString("Kupac: ");
			System.out.println();

			LocalDateTime datumIVreme = LocalDateTime.now();
			Karta karta = new Karta(0, voz, datumIVreme, kupac, razred);
			kartaDAO.add(karta);
			System.out.println("Karta je uspesno kupljena!");
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}


	

}
