package voz.ui;

import java.sql.Connection;
import java.sql.DriverManager;

import voz.DAO.KartaDAO;
import voz.DAO.VozDAO;
import voz.DAOImpl.KartaDAOImpl;
import voz.DAOImpl.VozDAOImpl;
import voz.util.Meni;
import voz.util.Meni.FunkcionalnaStavkaMenija;
import voz.util.Meni.IzlaznaStavkaMenija;
import voz.util.Meni.StavkaMenija;


public class Application {
	private static void initDatabase() throws Exception {
		Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/voz?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=Europe/Belgrade", 
				"root", 
				"root");
		
		VozDAO vozDAO = new VozDAOImpl(conn);
		KartaDAO kartaDAO = new KartaDAOImpl(conn);
		
		VozUI.setVozDAO(vozDAO);
		KartaUI.setKartaDAO(kartaDAO);
		IzvestajUI.setIzvestaj(vozDAO);
	}

	static {
		try {
			initDatabase();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Gre�ka pri povezivanju sa izvorom podataka!");
			
			System.exit(1);
		}
	}

	public static void main(String[] args) throws Exception {
		Meni.pokreni("Voz", new StavkaMenija[] {
			new IzlaznaStavkaMenija("Izlaz"),
			new FunkcionalnaStavkaMenija("Prikaz svih vozova") {

				@Override
				public void izvrsi() { VozUI.prikazSvih(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Prikaz jednog voza sa svim prodatim kartama") {

				@Override
				public void izvrsi() { VozUI.prikaz(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Prodaja karte") {

				@Override
				public void izvrsi() { KartaUI.dodavanje(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Izvestaj") {

				@Override
				public void izvrsi() { IzvestajUI.izvestavanje(); }
				
			}
		});
	}
}
