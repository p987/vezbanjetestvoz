package voz.ui;

import java.util.Collection;

import voz.DAO.VozDAO;
import voz.model.Karta;
import voz.model.Voz;
import voz.util.Konzola;

public class VozUI {

	private static VozDAO vozDAO;
	
	public static void setVozDAO(VozDAO vozDAO) {
		VozUI.vozDAO = vozDAO;
	}
	
	
	public static Voz pronalazenje() throws Exception {
		prikazSvih();

		String brojVoza = Konzola.ocitajString("Unesite broj voza: ");
		System.out.println();

		Voz voz = vozDAO.get(brojVoza);
		if (voz == null)
			Konzola.prikazi("Voz nije pronađeno!");

		return voz;
	}
	
	
	public static void prikaz() {
		try {
			Voz voz = pronalazenje();
			if (voz == null)
				return;
			
			System.out.println(voz);
			System.out.println("Broj preostalih slobodnih mesta je: " + (voz.getBrojMesta() - voz.getKartaSet().size()));
			System.out.println();
			for (Karta itKarta : voz.getKartaSet()) {
				System.out.println(itKarta);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}

	public static void prikazSvih() {
		try {
			Collection<Voz> vozKolekcija = vozDAO.getAll();

			System.out.println();
			for (Voz itVoz: vozKolekcija) {
				System.out.println(itVoz);
//				for (Karta itPorudzbina: itVoz.getKartaSet()) {
//					System.out.println(itPorudzbina);
//				}
//				System.out.println("\n\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}

	

	

}
