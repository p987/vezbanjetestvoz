package voz.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

import voz.DAO.KartaDAO;
import voz.DAO.VozDAO;
import voz.model.Karta;

public class KartaDAOImpl implements KartaDAO {
	
	private Connection conn;

	public KartaDAOImpl(Connection conn) {
		this.conn = conn;
	}

	@Override
	public void add(Karta karta) throws Exception {
		String sql = "INSERT INTO karte (vozId, datumIVremeProdaje, kupac, razred) VALUES (?, ?, ?, ?);";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			int param = 0;
			stmt.setLong(++param, karta.getVoz().getId());
			stmt.setTimestamp(++param, Timestamp.valueOf(karta.getDatumIVremeProdaje()));
			stmt.setString(++param, karta.getKupac());
			stmt.setInt(++param, karta.getRazred());
			

			stmt.executeUpdate();
		}
	}

}
