package voz.ui;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import voz.DAO.VozDAO;
import voz.model.StavkaIzvestaja;
import voz.model.Voz;
import voz.util.Konzola;

public class IzvestajUI {
	private static VozDAO vozDAO;

	public static void setIzvestaj(VozDAO vozDAO) {
		IzvestajUI.vozDAO = vozDAO;
	}
	
	public static void izvestavanje() {
		LocalDateTime pocetak = Konzola.ocitajDateTime("Unesite pocetak perioda");
		LocalDateTime kraj = Konzola.ocitajDateTime("Unesite kraj perioda");
//		LocalDateTime pocetak = LocalDateTime.parse("07.05.2022. 00:00", Konzola.getDateTimeFormatter());
//		LocalDateTime kraj =	LocalDateTime.parse("08.05.2022. 23:59", Konzola.getDateTimeFormatter());
		try {
			Collection<Voz> vozKolekcija = vozDAO.getAll();
			List<StavkaIzvestaja> stavkaIzvestajaList = new ArrayList<>();
			Map<String, StavkaIzvestaja> izvesatjMap = new LinkedHashMap<>();
			
			for (Voz itVoz : vozKolekcija) {
				if (itVoz.isDatumIVremeUOpsegu(pocetak, kraj)) {
					double ukupanPrihod = itVoz.getUkupanPrihod();
					StavkaIzvestaja izvestaj = new StavkaIzvestaja(itVoz, ukupanPrihod);
					StavkaIzvestaja izvestajPostoji = izvesatjMap.get(itVoz.getNaziv());
					if (izvestajPostoji != null) {
						if ((izvestajPostoji.voz.getKartaSet().size() - izvestaj.voz.getKartaSet().size()) > 0) {
							izvestaj = izvestajPostoji;
						}
					}
					izvesatjMap.put(izvestaj.voz.getNaziv(), izvestaj);
				}
			}
										
			// sortiranje izveštaja
			stavkaIzvestajaList.addAll(izvesatjMap.values());
			stavkaIzvestajaList.sort(StavkaIzvestaja::compareBrojPoziva);

			// prikaz izveštaja
			System.out.println();
			for (StavkaIzvestaja itStavka : stavkaIzvestajaList) {
				System.out.println(itStavka);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}
}
