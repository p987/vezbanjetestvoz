package voz.model;

import java.time.LocalDateTime;

import voz.util.Konzola;

public class StavkaIzvestaja {
	public final Voz voz;
	public final double ukupanPrihod;
	
	
	public static int compareBrojPoziva(StavkaIzvestaja stavka1, StavkaIzvestaja stavka2) {
		return -Double.compare(stavka1.ukupanPrihod, stavka2.ukupanPrihod);
	}
	


	public StavkaIzvestaja(Voz voz, double ukupanPrihod) {
		this.voz = voz;
		this.ukupanPrihod = ukupanPrihod;
	}
	


	@Override
	public String toString() {
		return "StavkaIzvestaja [nazivVoza=" + voz.getNaziv() + ", ukupanPrihod=" + ukupanPrihod
				+ ", najviseProdatihKarataDatumIVreme=" + Konzola.formatiraj(voz.getDatumIVremePolaska()) + "]";
	}



//	public String po() {
//		LocalDateTime nulto = LocalDateTime.parse("01.01.0001. 00:00", Konzola.getDateTimeFormatter());
//		return "StavkaIzvestaja [sifra=" + sifra + ", naziv=" + naziv + 
//				", poslednjiDatumIVreme=" + ((poslednjiDatumIVreme.compareTo(nulto) != 0)?(Konzola.formatiraj(poslednjiDatumIVreme)) : ("nepoznato")) + 
//				", ukupanPrihod=" + ukupanPrihod + "]";
//	}
}
