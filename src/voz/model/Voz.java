package voz.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import voz.util.Konzola;


public class Voz {

	private final long  id;
	private String broj;
	private String naziv;
	private LocalDateTime datumIVremePolaska;
	private double cenaKarte;
	private int brojMesta;
	private Set<Karta> kartaSet = new LinkedHashSet<>();
	
	
	public Voz(long id, String broj, String naziv, LocalDateTime datumIVremePolaska, double cenaKarte, int brojMesta) {
		this.id = id;
		this.broj = broj;
		this.naziv = naziv;
		this.datumIVremePolaska = datumIVremePolaska;
		this.cenaKarte = cenaKarte;
		this.brojMesta = brojMesta;
	}
	
	
	public boolean isDatumIVremeUOpsegu(LocalDateTime pocetak, LocalDateTime kraj) {
		return datumIVremePolaska.compareTo(pocetak) >= 0 && datumIVremePolaska.compareTo(kraj) <= 0;
	}
	
	
	public double getUkupanPrihod() {
		double ukupanPrihod = 0;	
		for (Karta karta : kartaSet) {
			if (karta.getRazred() == 2) {
				ukupanPrihod += cenaKarte * 0.85;
			} else {
				ukupanPrihod += cenaKarte;
			}
		}		
		return ukupanPrihod;
	}

	
	public Set<Karta> getKartaSet() {
		return kartaSet;
	}


	public void addAllKarta(Set<Karta> kartaSet) {
		this.kartaSet.addAll(kartaSet);
	}

	
	public void addKarta(Karta karta) {
		this.kartaSet.add(karta);
	}
	

	@Override
	public String toString() {
		return "Voz [id=" + id + ", broj=" + broj + ", naziv=" + naziv + ", datumIVremePolaska=" + Konzola.formatiraj(datumIVremePolaska)
				+ ", cenaKarte=" + cenaKarte + ", brojMesta=" + brojMesta + "]";
	}


	public String getBroj() {
		return broj;
	}


	public void setBroj(String broj) {
		this.broj = broj;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public LocalDateTime getDatumIVremePolaska() {
		return datumIVremePolaska;
	}


	public void setDatumIVremePolaska(LocalDateTime datumIVremePolaska) {
		this.datumIVremePolaska = datumIVremePolaska;
	}


	public double getCenaKarte() {
		return cenaKarte;
	}


	public void setCenaKarte(double cenaKarte) {
		this.cenaKarte = cenaKarte;
	}


	public int getBrojMesta() {
		return brojMesta;
	}


	public void setBrojMesta(int brojMesta) {
		this.brojMesta = brojMesta;
	}


	public long getId() {
		return id;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voz other = (Voz) obj;
		return id == other.id;
	}
	
	
	
}
